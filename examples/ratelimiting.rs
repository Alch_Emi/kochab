use std::time::Duration;

use anyhow::*;
use log::LevelFilter;
use kochab::{Server, Request, Response, Document};

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::builder()
        .filter_module("kochab", LevelFilter::Debug)
        .init();

    Server::new()
        .add_route("/", handle_request)
        .ratelimit("/limit", 2, Duration::from_secs(60))
        .serve_unix("kochab.sock")
        .await
}

async fn handle_request(request: Request) -> Result<Response> {
    let mut document = Document::new();

    if let Some("limit") = request.trailing_segments().get(0).map(String::as_str) {
        document.add_text("You're on a rate limited page!")
                .add_text("You can only access this page twice per minute");
    } else {
        document.add_text("You're on a normal page!")
                .add_text("You can access this page as much as you like.");
    }
    document.add_blank_line()
            .add_link("/limit", "Go to rate limited page")
            .add_link("/", "Go to a page that's not rate limited");
    Ok(document.into())
}
