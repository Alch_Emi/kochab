use anyhow::*;
use log::LevelFilter;
use kochab::{Server, Response, Document};
use kochab::document::HeadingLevel::*;

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::builder()
        .filter_module("kochab", LevelFilter::Debug)
        .init();

    let response: Response = Document::new()
        .add_preformatted_with_alt("kochab", include_str!("kochab_logo.txt"))
        .add_blank_line()
        .add_text(
            concat!(
            "Kochab is an extension & a fork of the Gemini SDK [northstar].  Where",
            " northstar creates an efficient and flexible foundation for Gemini projects,",
            " kochab seeks to be as ergonomic and intuitive as possible, making it",
            " possible to get straight into getting your ideas into geminispace, with no",
            " worrying about needing to build the tools to get there."
            )
        )
        .add_blank_line()
        .add_link("https://github.com/Alch-Emi/kochab", "GitHub")
        .add_blank_line()
        .add_heading(H2, "Usage")
        .add_blank_line()
        .add_text("Add the latest version of kochab to your `Cargo.toml`.")
        .add_blank_line()
        .add_preformatted_with_alt("toml", r#"kochab = { git = "https://github.com/Alch-Emi/kochab.git" }"#)
        .add_blank_line()
        .add_heading(H2, "Generating a key & certificate")
        .add_blank_line()
        .add_preformatted_with_alt("sh", concat!(
            "mkdir cert && cd cert\n",
            "openssl req -x509 -nodes -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365",
        ))
        .into();

    Server::new()
        .add_route("/", response)
        .serve_unix("kochab.sock")
        .await
}
