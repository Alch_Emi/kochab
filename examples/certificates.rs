use anyhow::Result;
use log::LevelFilter;

use std::fmt::Write;

use kochab::{Request, Response, Server};

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::builder()
        .filter_module("kochab", LevelFilter::Debug)
        .init();

    Server::new()
        .add_route("/", handle_request)
        .serve_unix("kochab.sock")
        .await
}

async fn handle_request(request: Request) -> Result<Response> {
    if let Some(fingerprint) = request.certificate() {
        let mut message = String::from("You connected with a certificate with a fingerprint of:\n");

        for byte in fingerprint {
            write!(&mut message, "{:x}", byte).unwrap();
        }

        Ok(Response::success_plain(message))
    } else {
        // The user didn't provide a certificate
        Ok(Response::client_certificate_required("You didn't provide a client certificate"))
    }
}
