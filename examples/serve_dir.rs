use std::path::PathBuf;

use anyhow::*;
use log::LevelFilter;
use kochab::Server;

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::builder()
        .filter_module("kochab", LevelFilter::Debug)
        .init();

    Server::new()
        .add_route("/", PathBuf::from("public")) // Serve directory listings & file contents
        .add_route("/about", PathBuf::from("README.md")) // Serve a single file
        .serve_unix("kochab.sock")
        .await
}
