```
      *.          ,.-*,,..
    .`  `.   .,-'`       ````--*,,,..
  .`      ;*`                       ```''-o
 *      ,' __              __          __
  `.  ,'  / /______  _____/ /_  ____ _/ /_
    ⭐   / //_/ __ \/ ___/ __ \/ __ `/ __ \
        / ,< / /_/ / /__/ / / / /_/ / /_/ /
       /_/|_|\____/\___/_/ /_/\__,_/_.___/
```
# kochab

Kochab is an extension & a fork of the Gemini SDK [northstar].  Where northstar creates an efficient and flexible foundation for Gemini projects, kochab seeks to be as ergonomic and intuitive as possible, making it possible to get straight into getting your ideas into geminispace, with no worrying about needing to build the tools to get there.

# Usage

It is currently only possible to use kochab through it's git repo, although it may wind up on crates.rs someday.

```toml
kochab = { git = "https://gitlab.com/Alch_Emi/kochab.git", branch = "stable" }
```

# Generating a key & certificate

By default, kochab enables the `certgen` feature, which will **automatically generate a certificate** for you.  All you need to do is run the program once and follow the prompts printed to stdout.  You can override this behavior by disabling the feature, or by using the methods in the `Builder`.

If you want to generate a certificate manually, it's recommended that you temporarily enable the `certgen` feature to do it, and then disable it once you're done, although you can also use the `openssl` client tool if you wish

[northstar]: https://github.com/panicbit/northstar "Northstar GitHub"
