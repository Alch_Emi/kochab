# Username Exists

Unfortunately, it looks like the username {username} is already taken.  If this is your account, and you have a password set, you can link this certificate to your account.  Otherwise, please choose another username.

=> /account/register Choose a different username
=> /account/login/{username} Link this certificate
