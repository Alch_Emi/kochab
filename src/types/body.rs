use tokio::io::AsyncRead;
#[cfg(feature="scgi_srv")]
use tokio::io::AsyncReadExt;
#[cfg(feature="serve_dir")]
use tokio::fs::File;

use std::borrow::Borrow;

use crate::types::Document;

pub enum Body {
    Bytes(Vec<u8>),
    Reader(Box<dyn AsyncRead + Send + Sync + Unpin>),
}

impl Body {
    /// Called by [`Response::rewrite_all`]
    #[cfg(feature="scgi_srv")]
    pub (crate) async fn rewrite_all(&mut self, based_on: &crate::Request) -> std::io::Result<bool> {
        let bytes = match self {
            Self::Bytes(bytes) => {
                let mut newbytes = Vec::new();
                std::mem::swap(bytes, &mut newbytes);
                newbytes
            }
            Self::Reader(reader) => {
                let mut bytes = Vec::new();
                reader.read_to_end(&mut bytes).await?;
                bytes
            }
        };
        let mut body = String::from_utf8(bytes).expect("text/gemini wasn't UTF8");

        let mut maybe_indx = if body.starts_with("=> ") {
            Some(3)
        } else {
            body.find("\n=> ").map(|offset| offset + 4)
        };
        while let Some(indx) = maybe_indx {

            // Find the end of the link part
            let end = (&body[indx..]).find(&[' ', '\n', '\r'][..])
                .map(|offset| indx + offset )
                .unwrap_or(body.len());

            // Perform replacement
            if let Some(replacement) = based_on.rewrite_path(&body[indx..end]) {
                body.replace_range(indx..end, replacement.as_str());
            } else {
                return Ok(false)
            };

            // Find next match
            maybe_indx = (&body[indx..]).find("\n=> ").map(|offset| offset + 4 + indx);
        }

        *self = Self::Bytes(body.into_bytes());
        Ok(true)
    }
}

impl<D: Borrow<Document>> From<D> for Body {
    fn from(document: D) -> Self {
        Self::from(document.borrow().to_string())
    }
}

impl From<Vec<u8>> for Body {
    fn from(bytes: Vec<u8>) -> Self {
        Self::Bytes(bytes)
    }
}

impl<'a> From<&'a [u8]> for Body {
    fn from(bytes: &[u8]) -> Self {
        Self::Bytes(bytes.to_owned())
    }
}

impl From<String> for Body {
    fn from(text: String) -> Self {
        Self::Bytes(text.into_bytes())
    }
}

impl<'a> From<&'a str> for Body {
    fn from(text: &str) -> Self {
        Self::Bytes(text.to_owned().into_bytes())
    }
}

#[cfg(feature="serve_dir")]
impl From<File> for Body {
    fn from(file: File) -> Self {
        Self::Reader(Box::new(file))
    }
}
